class machine{
        turnOn () {console.log('Механизм работает')};
        turnOff () {console.log('Механизм не работает')};
}

class HomeAppliance extends machine{
		turnOn () {
			super.turnOn();
		};
    	plugIn () {console.log('Включить в сеть')};
		turnOff () {
			super.turnOff();
		};
    	plugOff () {console.log('Выключить из сети')};
}

class WashingMachine extends HomeAppliance{
		plugIn () {
			super.plugIn ();
		}
    	run() {console.log('Запуск случился!')};
}

class LightSource extends HomeAppliance{
   	plugIn() {
        super.plugIn();
    }
    setLevel(setLevel) {
   	    if (setLevel < 1 || setLevel > 100) {
   	        console.log('Неполадки с освещением')
        } else {
            console.log(`Установить уровень освещенности ${setLevel}`)
        }
   	};
}

class AutoVehicle extends machine{
		position() {
            this.x = 0;
            this.y = 0;
            console.log(`Координаты начальные: ${this.x} ${this.y}`)
		}
        setPosition(x, y) {
            this.x = x;
            this.y = y;
            console.log(`Текущие координаты: ${this.x}  ${this.y}`);
        };
}

class Car extends AutoVehicle{
    	speedNow () {
            this.speed = 10;
            console.log(`Текущая скорость ${this.speed}`);
		};
        setSpeed (speed) {
            this.speed = speed;
            console.log(`Заданная скорость ${this.speed}`);
        };
        run(x, y) {
            console.log(`Координаты введены: ${x} ${y}`);
            const interval = setInterval(() => {
                let xCoordination = this.x + this.speed;
           		let yCoordination = this.y + this.speed;
                if (xCoordination >= x) {
                	xCoordination = x;
                }
                if (yCoordination >= y) {
                	yCoordination = y;
                }
                this.setPosition(xCoordination, yCoordination);
                if (xCoordination === x && yCoordination === y) {
                	clearInterval(interval);
                }
            }, 1000)
        };
}